Django==3.2.7
djangorestframework==3.12.4
djangorestframework-simplejwt==4.7.2
django-mysql==4.0.0
mysqlclient==2.0.3
Pillow==8.3.2