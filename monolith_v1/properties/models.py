from django.db import models


class Status(models.Model):
    name = models.CharField(
        max_length=32, verbose_name="Nombre"
    )
    label = models.CharField(
        max_length=64, verbose_name="Label"
    )

    class Meta:
        db_table = "status"
        verbose_name = 'Estado'
        verbose_name_plural = 'Estados'

    def __str__(self):
        return f"{self.name}"



class Property(models.Model):
    address = models.CharField(
        max_length=120, verbose_name="Dirección"
    )
    city = models.CharField(
        max_length=32, verbose_name="Ciudad"
    )
    price = models.BigIntegerField(
        verbose_name="Precio"
    )
    description = models.TextField(
        verbose_name="Descripción",
        blank=True,
        null=True,
    )
    year = models.IntegerField(
        verbose_name="Año",
        blank=True,
        null=True,
    )
    states = models.ManyToManyField(Status, through="StatusHistory")

    class Meta:
        db_table = "property"
        verbose_name = 'Propiedad'
        verbose_name_plural = 'Propiedades'

    def has_status(self):
        print(self.status.filter(property__id=1))

    def __str__(self):
        return str(f"{self.id}")



class StatusHistory(models.Model):
    property = models.ForeignKey(
        Property, on_delete=models.CASCADE, verbose_name="Propiedad"
    )
    status = models.ForeignKey(
        Status, on_delete=models.CASCADE, verbose_name="Estado"
    )
    update_date = models.DateTimeField(
        auto_now=True, verbose_name='Fecha de actualización',
        blank=True, null=True
    )

    class Meta:
        db_table = "status_history"
        verbose_name = 'Historial de Estado'
        verbose_name_plural = 'Historial de Estados'

    def __str__(self):
        return str(f"La propierdad {self.property} esta en: {self.status}")