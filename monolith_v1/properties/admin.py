from django.contrib import admin
from .models import Property, Status, StatusHistory

@admin.register(Property)
class PropertyAdmin(admin.ModelAdmin):
    list_display = ('address', 'city', 'price', 'description', 'year', )
    search_fields = ['address', 'city', 'year', ]

@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    list_display = ('name', 'label', )
    search_fields = ['name', 'label', ]

@admin.register(StatusHistory)
class StatusHistoryAdmin(admin.ModelAdmin):
    list_display = ('property', 'status', )
    search_fields = ['property', 'status', ]