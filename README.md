PROPERTI
==========

Properti backend on Django with Docker

Requerimientos
--------------

### Librerías

* [Docker: ](https://docs.docker.com/install "Ver documentación") Instale Docker para diferentes sistemas operativos.
* [Docker Compose: ](https://docs.docker.com/compose/install/ "Ver documentación") Instale Docker Compose para MacOS, Windows y Linux.
* [Python 3.8: ](https://www.python.org/ "Ver documentación") Lenguaje de programación.
* [Django: ](https://www.djangoproject.com/ "Ver documentación") Los paquetes de requisitos de django están en el archivo requirements.txt.
* [MYSQL: ](https://www.mysql.com// "Ver documentación") Las credenciales de usuario están en el archivo de variable de entorno llamado: .env.
* [Nginx: ](https://nginx.org/en/ "Ver documentación") Nginx se utiliza como proxy inverso.
* [Redis: ](https://redis.io/ "Ver documentación") Redis se utiliza como worker para atender tareas asíncronas y cache.
* [Django Rest Framework: ](https://www.django-rest-framework.org/ "Ver documentación") Django Rest Framework es una herramienta para crear API web.

PROPERTY LOCAL
================

* **Build or rebuild web project**: ``$ docker-compose build``
* **Start web project as a service in docker**: ``$ docker-compose up``
* **Stops and removes containers, networks, volumes, and images in docker**: ``$ docker-compose down``
* **Create Django project**: ``$ docker-compose run web django-admin.py startproject <project_name> .``
* **Launch the development server**: ``$ docker-compose run web python manage.py runserver 0.0.0.0:8000``
* **Run makemigrations command on Django project**: ``$ docker-compose run web python manage.py makemigrations``
* **Run migrate command on Django project**: ``$ docker-compose run web python manage.py migrate``
* **Create superuser on Django project**: ``$ docker-compose run web python manage.py createsuperuser``
* **Run test command on Django project**: ``$ docker-compose run web python manage.py test``

#### Contenido del env.template
```bash
SECRET_KEY=SECRET
MYSQL_DATABASE=habi_db
MYSQL_USER=admin
MYSQL_PASSWORD=root
MYSQL_ROOT_PASSWORD=root
DB_HOST=db
DB_PORT=3306
```
